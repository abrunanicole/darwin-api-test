## Darwin Api Test

Projeto de automação do Desafio Zup para vaga de QA.

### Tecnologias

* Java 8
* Gradle
* Rest Assured

### Execução dos testes

`export API_TEST_USER={user}`

`export API_TEST_PASSWORD={password}`

`./gradlew clean test`

### Relatório de execução

O relatório é gerado automaticamente após a execução do teste, e fica no endereço:  

`darwin-api-test/build/reports/tests/test/index.html`

### Observações

O endpoint getBuild está retornando 404. O teste foi escrito esperando que a resposta seja 200, por isso esse teste falha até que isso seja corrigido na api.
