package com.darwin.apiTest;

import com.darwin.apiTest.utils.BaseTest;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasLength;

public class LoginTest extends BaseTest {
    private String url;

    @Before
    public void setup() {
        url = getDarwinLoginUri() + "auth/realms/darwin/protocol/openid-connect/token";
    }

    @Test
    public void shouldAuthenticateSuccessfully(){
        given()
            .header("Content-Type", "application/x-www-form-urlencoded")
            .formParam("username", getUsername())
            .formParam("password", getPassword())
            .formParam("grant_type", "password")
            .formParam("client_id", "darwin-client")
        .when()
            .post(url)
        .then()
            .assertThat()
            .statusCode(HttpStatus.SC_OK)
            .body("access_token", hasLength(1534))
            .body("refresh_token", hasLength(967))
            .body("expires_in", equalTo(3600))
            .body("refresh_expires_in", equalTo(1800))
            .body("token_type", equalTo("bearer"))
            .body("not-before-policy", equalTo(0))
            .body("session_state", hasLength(36))
            .body("scope", equalTo("profile email"));
    }

    @Test
    public void shouldNotAuthenticateWhenThePasswordIsIncorrect(){
        given()
            .header("Content-Type", "application/x-www-form-urlencoded")
            .formParam("username", getUsername())
            .formParam("password", "wrongPassword")
            .formParam("grant_type", "password")
            .formParam("client_id", "darwin-client")
        .when()
            .post(url)
        .then()
            .assertThat()
            .statusCode(HttpStatus.SC_UNAUTHORIZED)
            .body("error", equalTo("invalid_grant"))
            .body("error_description", equalTo("Invalid user credentials"));
    }

    @Test
    public void shouldNotAuthenticateWhenTheGrantTypeIsMissing(){
        given()
            .header("Content-Type", "application/x-www-form-urlencoded")
            .formParam("username", getUsername())
            .formParam("password", getPassword())
            .formParam("client_id", "darwin-client")
        .when()
            .post(url)
        .then()
            .assertThat()
            .statusCode(HttpStatus.SC_BAD_REQUEST)
            .body("error", equalTo("invalid_request"))
            .body("error_description", equalTo("Missing form parameter: grant_type"));
    }

    @Test
    public void shouldNotAuthenticateWhenTheClientIdIsMissing(){
        given()
            .header("Content-Type", "application/x-www-form-urlencoded")
            .formParam("username", getUsername())
            .formParam("password", getPassword())
            .formParam("grant_type", "password")
        .when()
            .post(url)
        .then()
            .assertThat()
            .statusCode(HttpStatus.SC_BAD_REQUEST)
            .body("error", equalTo("unauthorized_client"))
            .body("error_description", equalTo("INVALID_CREDENTIALS: Invalid client credentials"));
    }
}
