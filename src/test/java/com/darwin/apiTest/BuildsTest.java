package com.darwin.apiTest;

import com.darwin.apiTest.utils.BaseTest;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class BuildsTest extends BaseTest{
    private String url;
    private String token;
    private static final Object EMPTY_STRING = "";

    @Before
    public void setup() {
        url = getDarwinUri() + "moove/builds";
        token = getAccessToken();
    }

    @Test
    public void shouldGetBuildsSuccessfully(){
        given()
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .header("Connection", "keep-alive")
                .header("x-application-id", "900c4342-df7e-488b-bf6f-1784ee7c0546")
                .header("Accept", "application/json, text/plain, */*")
                .header("Origin", "https://darwin.continuousplatform.com")
                .header("Sec-Fetch-Site", "same-site")
                .header("Sec-Fetch-Mode", "cors")
                .header("Referer", "https://darwin.continuousplatform.com/circles/d83b4772-d958-49e3-9bf6-0d1e6735d405/edit")
                .header("Accept-Encoding", "gzip, deflate, br")
                .param("tagName", EMPTY_STRING)
                .param("page", 0)
                .param("status", "BUILT")
                .when()
                .get(url)
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void shouldNotCreateCircleWhenApplicationIdIsNotSent() {
        given()
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .header("Connection", "keep-alive")
                .header("Accept", "application/json, text/plain, */*")
                .header("Origin", "https://darwin.continuousplatform.com")
                .header("Sec-Fetch-Site", "same-site")
                .header("Sec-Fetch-Mode", "cors")
                .header("Referer", "https://darwin.continuousplatform.com/circles/d83b4772-d958-49e3-9bf6-0d1e6735d405/edit")
                .header("Accept-Encoding", "gzip, deflate, br")
                .param("tagName", EMPTY_STRING)
                .param("page", 0)
                .param("status", "BUILT")
                .when()
                .get(url)
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                .body("timestamp", notNullValue())
                .body("status", equalTo(500))
                .body("error", equalTo("Internal Server Error"))
                .body("message", equalTo("br.com.zup.darwin.security.constraint.SecurityConstraintsException: Missing application ID header"))
                .body("path", equalTo("/builds"));
    }

    @Test
    public void shouldNotCreateCircleWhenTokenIsNotSent() {
        given()
                .header("Content-Type", "application/json")
                .header("Connection", "keep-alive")
                .header("Accept", "application/json, text/plain, */*")
                .header("Origin", "https://darwin.continuousplatform.com")
                .header("Sec-Fetch-Site", "same-site")
                .header("Sec-Fetch-Mode", "cors")
                .header("Referer", "https://darwin.continuousplatform.com/circles/d83b4772-d958-49e3-9bf6-0d1e6735d405/edit")
                .header("Accept-Encoding", "gzip, deflate, br")
                .param("tagName", EMPTY_STRING)
                .param("page", 0)
                .param("status", "BUILT")
                .when()
                .get(url)
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_UNAUTHORIZED)
                .body("error", equalTo("Key not authorised"));
    }
}
