package com.darwin.apiTest.utils;

import static io.restassured.RestAssured.given;

public class BaseTest {
    private final String darwinUri = "https://darwin-api.continuousplatform.com/";
    private final String darwinLoginUri = "https://darwin-keycloak.continuousplatform.com/";
    private String userName;
    private String password;

    public String getDarwinUri() { return this.darwinUri; }
    public String getDarwinLoginUri() { return this.darwinLoginUri; }
    public String getUsername() { return this.userName; }
    public String getPassword() { return this.password; }

    public BaseTest() {
        this.userName = System.getenv("API_TEST_USER");
        this.password = System.getenv("API_TEST_PASSWORD");
    }

    public String getAccessToken() {
        String url = this.darwinLoginUri + "auth/realms/darwin/protocol/openid-connect/token";
        return given()
            .header("Content-Type", "application/x-www-form-urlencoded")
            .formParam("username", this.userName)
            .formParam("password", this.password)
            .formParam("grant_type", "password")
            .formParam("client_id", "darwin-client")
        .when()
            .post(url)
        .then()
            .extract()
            .path("access_token");
    }
}
