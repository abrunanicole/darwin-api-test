package com.darwin.apiTest;

import com.darwin.apiTest.utils.BaseTest;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class CirclesTest extends BaseTest{
    private String url;
    private String token;
    private static final Object EMPTY_STRING = "";
    private static final String AUTHOR_NAME = "processoqa";
    private static final String AUTHOR_ID = "5e397f95-11af-4512-a0d7-c68b840e818b";
    private static final String NAME = "My Circle";
    private static final String RULE_MATCHER_TYPE = "DARWIN";
    private static final String RULE_TYPE = "CLAUSE";
    private static final String RULE_LOGICAL_OPERATOR = "OR";
    private static final String RULE_CLAUSE_TYPE = "RULE";
    private static final String RULE_CLAUSE_KEY = "username";
    private static final String RULE_CLAUSE_CONDITION = "EQUAL";

    @Before
    public void setup() {
        url = getDarwinUri() + "moove/circles";
        token = getAccessToken();

    }

    @Test
    public void shouldCreateCircleSuccessfully(){
        JSONObject requestBody = new JSONObject();

        requestBody.put("name", NAME)
            .put("ruleMatcherType", RULE_MATCHER_TYPE)
            .put("rules", rules())
            .put("segmentations", new ArrayList<>())
            .put("authorId", AUTHOR_ID);

        given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(requestBody.toString())
                .when()
                .post(url)
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("id", hasLength(36))
        .body("name", equalTo(NAME))
                .body("author.id", equalTo(AUTHOR_ID))
                .body("author.name", equalTo(AUTHOR_NAME))
                .body("author.email", equalTo("processoqa@zup.com.br"))
                .body("author.photoUrl", equalTo(EMPTY_STRING))
                .body("author.createdAt", isA(String.class))
                .body("createdAt", isA(String.class))
                .body("matcherType", equalTo("REGULAR"))
                .body("rules.type", equalTo(RULE_TYPE))
                .body("rules.logicalOperator", equalTo(RULE_LOGICAL_OPERATOR))
                .body("rules.clauses[0].type", equalTo(RULE_CLAUSE_TYPE))
                .body("rules.clauses[0].content.key", equalTo(RULE_CLAUSE_KEY))
                .body("rules.clauses[0].content.condition", equalTo(RULE_CLAUSE_CONDITION))
                .body("rules.clauses[0].content.value[0]", equalTo(AUTHOR_NAME))
                .body("deployment", equalTo(null))
                .body("importedAt", equalTo(null))
                .body("importedKvRecords", equalTo(null));
    }

    @Test
    public void shouldNotCreateCircleWhenRulesIsNotSent() {
        JSONObject requestBody = new JSONObject();

        requestBody.put("name", NAME)
                .put("ruleMatcherType", RULE_MATCHER_TYPE)
                .put("segmentations", new ArrayList<>())
                .put("authorId", AUTHOR_ID);

        String token = getAccessToken();
        given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(requestBody.toString())
                .when()
                .post(url)
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("code", equalTo("PAYLOAD_INVALID"))
                .body("message", equalTo("payload.invalid"));
    }

    @Test
    public void shouldNotCreateCircleWhenTokenIsNotSent() {
        JSONObject requestBody = new JSONObject();

        requestBody.put("name", NAME)
                .put("ruleMatcherType", RULE_MATCHER_TYPE)
                .put("rules", rules())
                .put("segmentations", new ArrayList<>())
                .put("authorId", AUTHOR_ID);

        given()
                .header("Content-Type", "application/json")
                .body(requestBody.toString())
                .when()
                .post(url)
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_UNAUTHORIZED)
                .body("error", equalTo("Key not authorised"));
    }

    private JSONObject rules() {
        return new JSONObject()
                .put("type", RULE_TYPE)
                .put("clauses", new ArrayList<>())
                .append("clauses", clause())
                .put("logicalOperator", RULE_LOGICAL_OPERATOR);
    }

    private JSONObject clause() {
        JSONObject content = new JSONObject()
                .put("key", RULE_CLAUSE_KEY)
                .put("value", new ArrayList<>())
                .append("value", AUTHOR_NAME)
                .put("condition", RULE_CLAUSE_CONDITION);

        return new JSONObject()
                .put("type", RULE_CLAUSE_TYPE)
                .put("content", content);
    }
}
